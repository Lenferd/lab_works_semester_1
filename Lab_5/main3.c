/*
	��������� ��� ���������� ������:
	�������� ���������, � ������� ��������� ��������� BOOK ��� �������� ���������� � ��������� ����, ��� ������� � ���� �������. 
	�� ���������� �����, ��������������� � ������� ���������, ����������� ������ � ������ � ������������ ������ �������� BOOK.
	3) ����������� ������, ��������������� �� �������� �������
*/

#include <stdio.h>
#include <locale.h>
#include <stdlib.h>
#include <string.h>

typedef struct		//��������� ��� ����
{
	char title[30];
	char author[30];
	int year;
} BOOK;

void print(BOOK *ptr) 
{
	printf("Title:%s", ptr->title);
	printf("Author:%s", ptr->author);
	printf("Year:%d\n", ptr->year);
	printf("\n");
}

int main()
{
	FILE *file;
	BOOK *arr;
	int count = 10, i = 0, j, k, word = 1;
	char buf[50];

	setlocale(LC_ALL, "rus");

	file = fopen("books_data.txt", "r");
	if (file == NULL)
	{
		perror("ERROR. FILE NOT FOUND!");
		exit(1);
	}

	arr = (BOOK*)malloc(count*sizeof(BOOK));
	while (fgets(buf, 50, file))
	{
		if (i == count - 1)
			arr = (BOOK*)realloc(arr, sizeof(BOOK)*(count += 10));
		if (word == 1)
		{
			strcpy(arr[i].title, buf);
			word++;
		}
		else if (word == 2)
		{
			strcpy(arr[i].author, buf);
			word++;
		}
		else if (word == 3)
		{
			arr[i].year = atoi(buf);
			word = 1;
			i++;
		}
	}

	for(j = 0 ; j < i - 1; j++) 
       for(k = 0 ; k < i - j - 1 ; k++) 
           if (strcmp(arr[k].author, arr[k+1].author)>0) 
		   {
			   BOOK tmp = arr[k]; arr[k] = arr[k+1]; arr[k+1] = tmp;
		   }
	for (j = 0; j < i; j++)
		print(&arr[j]);
	return 0;
}