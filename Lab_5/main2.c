/*
	��������� ��� ���������� ������:
	�������� ���������, � ������� ��������� ��������� BOOK ��� �������� ���������� � ��������� ����, ��� ������� � ���� �������. 
	�� ���������� �����, ��������������� � ������� ���������, ����������� ������ � ������ � ������������ ������ �������� BOOK.
	2) ����������� ������ � ����� ������ � ����� ����� �����
*/

#include <stdio.h>
#include <locale.h>
#include <stdlib.h>
#include <string.h>

typedef struct		//��������� ��� ����
{
	char title[30];
	char author[30];
	int year;
} BOOK;

void print(BOOK *ptr) 
{
	printf("Title:%s", ptr->title);
	printf("Author:%s", ptr->author);
	printf("Year:%d\n", ptr->year);
	printf("\n");
}

int main()
{
	int count = 10, i = 0, j, word = 1;
	char buf[50];
	int old = 0, neww = 0, minyear = 3000, maxyear = -1;
	BOOK *arr;
	FILE *file;

	setlocale(LC_ALL, "rus");
	
	file = fopen("books_data.txt", "r");
	if (file == NULL)
	{
		perror("ERROR. FILE NOT FOUND!");
		exit(1);
	}

	arr = (BOOK*)malloc(count*sizeof(BOOK));
	while (fgets(buf, 50, file))
	{
		if (i == count - 1)
			arr = (BOOK*)realloc(arr, sizeof(BOOK)*(count += 10));
		if (word == 1)
		{
			strcpy(arr[i].title, buf);
			word++;
		}
		else if (word == 2)
		{
			strcpy(arr[i].author, buf);
			word++;
		}
		else if (word == 3)
		{
			arr[i].year = atoi(buf);
			if (arr[i].year > maxyear)
			{
				maxyear = arr[i].year;
				neww = i;
			}
			if (arr[i].year < minyear)
			{
				minyear = arr[i].year;
				old = i;
			}
			word = 1;
			i++;
		}
	}
	puts("newest book:");
	print(&arr[neww]);
	puts("oldest book:");
	print(&arr[old]);
	return 0;
}