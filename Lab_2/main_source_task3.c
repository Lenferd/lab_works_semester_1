/*
	��������� ��� ���������� ������:
	�������� ���������, ������������� � ������������ ������ � ��������� �� ����� ������� ������������� �������� ��� ��������� ������. 
	������� ������������ �������� � ������� �������� �� �����.
*/

#include <stdio.h>
#include <locale.h>
#include <string.h>

#define MAXSTRING 257

int main ()
{
	int i=0;
	char Counter[MAXSTRING]={0};
	char String[MAXSTRING];

	setlocale (LC_ALL, "rus");

	printf ("������� ������ (�� ����� 256 ��������.)\n");		
	while (1)			//�������� �� �� ���������� ������� ������
	{
		fflush(stdin);
		if ((fgets(String,MAXSTRING,stdin)!=0) && (strlen(String)<MAXSTRING))
			break;
		else 
			printf ("������, ��������� ����.\n");
	}

	for (i=0; i<strlen(String)-1; i++)		
	{
		if (String[i]!=' ')			//������ �� ������� �� ������
			Counter[String[i]]++;	//������� �������� Counter[����� �������]
	}

	for (i=0; i<MAXSTRING; i++)
	{
		if (Counter[i]!=0)		//������� ���-�� ��� ���� ��������, ����� ���, ������� �� �������������� � ������.
			printf ("����� �������� %c \t %d\n", i, Counter[i]);
	}
	return 0;
}