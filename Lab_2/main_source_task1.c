/*
	��������� ��� ���������� ������:
	�������� ���������, ������������ ������, ��������� �� ��������� ���� (� ����� ���������) � ����.
	����� ������ �������� � ����������.
*/

#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <time.h>

#define MAXPASS 101

int main()
{
	int randnum;
	int LenghtPass, i;
	char Password[MAXPASS];
	setlocale(LC_ALL,"rus");
	srand(time(0));

	printf("������� ����� ������ (�� ����� 100 ��������): ");
	scanf("%d", &LenghtPass);
	Password[LenghtPass]=0;

	for (i=0;i<LenghtPass;i++)
	{
		switch (rand()%3+1)
		{
			case 1: Password[i]=(rand() %('0'-'9')+'0');
				break;
			case 2: Password[i]=(rand() %('A'-'Z')+'A');
				break;
			case 3: Password[i]=(rand() %('a'-'z')+'a');
				break;
		}
	}
	printf ("��� ������: %s\n", Password);
	return 0;
}