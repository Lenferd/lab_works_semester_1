/*
	��������� ��� ���������� ������:
	�������� ���������, ������������ ��������� ������� �������� �����. 
	����� ������ � ���������� ������� �������� � ����������.
*/
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <time.h>

#define MAXPASS 101

int main()
{
	int randnum;
	int LenghtPass, PassCount, i,j;
	char Password[MAXPASS];
	setlocale(LC_ALL,"rus");
	srand(time(0));

	printf("������� ����� ������ (�� ����� 100 ��������): ");
	scanf("%d", &LenghtPass);
	printf("������� ���-�� �������: ");
	scanf("%d", &PassCount);

	Password[LenghtPass]=0;
	for (j=0;j<PassCount;j++)
	{
		for (i=0;i<LenghtPass;i++)
		{
			switch (rand()%3+1)
			{
				case 1: Password[i]=(rand() %('0'-'9')+'0');
					break;
				case 2: Password[i]=(rand() %('A'-'Z')+'A');
					break;
				case 3: Password[i]=(rand() %('a'-'z')+'a');
					break;
			}
		}
		printf ("��� ������ %d: %s\n",(j+1), Password);
	}
	return 0;
}