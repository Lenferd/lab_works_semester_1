/*
	������� ������, ���������� ���������� ������������� �����, �������� "12345". 
	������������� � ������, ��� ������ �������� �������� ���������, �� ���� "12 345"
*/

#include <stdio.h>
#include <locale.h>

#define MAXSTRING 100

int main()
{
	char string[MAXSTRING];
	char stringFin[MAXSTRING];
	char *point;
	int stringlength=0, stringlengthfin=0;
	int i=0, count=0;
	setlocale (LC_ALL, "rus");

	fgets(string, MAXSTRING, stdin);
	point=string;
	while (*point!='\n')		//������, ������� �������� � ������
	{
		stringlength++;
		point++;
	}
	for (--point; 1; point--)
	{
		if (stringlength==0)			//�������� �� ����� ������, ���� �����, �� ���������� ����� � ��� ������������ ������ (stringFin)
		{
			stringFin[i]=0;
			break;
		}
		stringFin[i]=*point;
		i++;
		count++;
		stringlength--;
		if (count==3)
		{
			stringFin[i++]=(' '); //stringFin[i++]=('\'') ��� ���������� ����������
			count=0;
		}
	} 
	printf("���������:\t");
	for (i;i>=0;i--)
	{
		printf("%c", stringFin[i]);
	}
	printf("\n");
	return 0;
}