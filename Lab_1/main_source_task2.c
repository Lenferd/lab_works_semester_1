/*
	��������� ��� ���������� ������:
	�������� ���������, ����������� �������� ����� � ������������ ������� (����'�����) 
	� ��������� � ����������� (��). � ����� ���� 12 ������, � 1 ����� 2.54 ��. 
	��� ������������ ����� ��������� ������ �������� ��������� �� ������.
*/

#include <stdio.h>
#include <locale.h>

#define INCHTOSM 2.54f
#define FTTOSM 12
int main ()
{
	unsigned int LengthFt, LengthInch; //������� ������, ����\�����
	unsigned int LengthSm;	//������ �� �����, ����������
	setlocale (LC_ALL, "rus");

	printf ("������� ���� ���� � ������������ �������\n������: 5'54\n\t");
	while (1)
	{
		fflush(stdin);
		scanf ("%d'%d", &LengthFt, &LengthInch);
		if (LengthFt>9 || LengthFt<2 || LengthInch>99) 
			printf ("������ ������� �� �����, ��������� ����.\n\t");
		else 
		{
			LengthSm = (LengthFt*FTTOSM + LengthInch)*INCHTOSM;
			printf("\n\t��� ���� � �����������: %d\n", LengthSm);		//���������� ����� ������ ����� ��������
			break;
		}
	}
	return 0;
}