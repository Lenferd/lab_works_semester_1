#include "Automata.h"


Automata::Automata(void)
{
	cash = 0;
	char buf[51];
	int count = 10, i = 0;
	int flag = 1;
	numberofdrinks = 0;
	state = OFF;
	FILE *fp;
	fp = fopen("drinks.txt", "r");
	if (fp == NULL)
	{
		perror("File error!");
		exit(1);
	}
	while (fgets(buf, 51, fp))
	{
		numberofdrinks++;
	}
	numberofdrinks /= 2;
	rewind(fp);
	menu = new MENU[numberofdrinks];
	for (int i = 0; i < numberofdrinks; i++)
	{
		fgets(buf, 51, fp);
		buf[strlen(buf) - 1] = 0;
		strcpy(menu[i].drink, buf);
		fgets(buf, 51, fp);
		menu[i].price = atoi(buf);
	}
}

void Automata::on()
{
	if (state != OFF)
		printf("Not avaiable");
	else
	{
		state = WAIT;
		printState();
	}
}

void Automata::off()
{
	if (state != WAIT)
		printf("Not avaiable");
	else
	{
		state = OFF;
		printState();
	}
}

void Automata::coin(int value)
{
	cash += value;
	state = ACCEPT;
	printState();
	printf("Money: %d\n", cash);
}

void Automata::printMenu()
{
	for (int i = 0; i < numberofdrinks; i++)
	{
		printf("%d: %s, price: %d\n", i, menu[i].drink, menu[i].price);
	}
}

STATES Automata::getState()
{
	return state;
}

void Automata::printState()
{
	switch (state)
	{
	case 0: printf("status: OFF"); break;
	case 1: printf("status: WAIT"); break;
	case 2: printf("status: ACCEPT"); break;
	case 3: printf("status: CHECK"); break;
	case 4: printf("status: COOK"); break;
	}
}

void Automata::choice(int ch)
{
	choose = ch;
}

bool Automata::check()
{
	state = CHECK;
	printState();
	if (cash < menu[choose].price)
	{
		printf("Not enough money");
		cancel();
		return false;
	}
	else if (cash == menu[choose].price)
	{
		printf("Everything fine!");
		return true;
	}
	else if (cash > menu[choose].price)
	{
		printf("Here's your change: %d\n", cash - menu[choose].price);
		cash = 0;
		return true;
	}
}

void Automata::cancel()
{
	if ((state == ACCEPT) | (state == CHECK))
	{
		state = WAIT;
		printf("return the money (%d) to you\n", cash);
		cash = 0;
	}
	else printf("Not Avaiable");
}

void Automata::cook()
{
	state = COOK;
	printState();
	printf("Wait until your drink will be ready");
}

void Automata::finish()
{
	printf("Enjoy your drink! Come again!");
	state = WAIT;
	printState();
}