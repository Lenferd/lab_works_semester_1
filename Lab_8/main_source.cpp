/*
	������� ������� ������������� �������� �� ������� ������� �������� (���, ����, ������ � �.�.) � ������� ��� ������ � ���� ��������� ���������.
	������� ������������ ��� STATES ��� ������� ��������� ��������
	������� ����� Automata, �������� � ���� ����������:
	cash - ��� �������� ������� �����; 
	menu - ������ ����� �������� �������� (����� ������������ �� �����); 
	prices - ������ ��� �������� (������������� ������� menu); 
	state - ������� ��������� ��������;
*/

#include "Automata.h"
#include <stdio.h>
int main()
{
	Automata vendmach1; 
	vendmach1.printState();
	bool shutdown = false;
	int value, x;
	while (shutdown != true)
	{
		printf("0 - ON\n1 - Insert money\n2 - OFF\n3 - Shut Down");
		scanf("%d", &x);
		switch (x)
		{
		case 0: vendmach1.on(); break;
		case 1: 
			if (vendmach1.getState() != WAIT)
				{
				printf("Not avaiable");
				break;
				}
			else
				{
					printf("Put the money into the coin acceptor");
					do
					{
						scanf("%d", &value);
						vendmach1.coin(value);
					} while (value!=0);
					printf("Choose the drink");
					vendmach1.printMenu(); scanf("%d", &value); 
					vendmach1.choice(value);
					if (vendmach1.check() == true)
						{
							vendmach1.cook();
							vendmach1.finish();
						}
					break;
				}
		case 2: vendmach1.off(); break;
		case 3: 
			if (vendmach1.getState() != OFF)
				{
					printf("Not avaiable");
					break;
				}
			else shutdown = true;
		}
	}
	return 0;
}
