/*
	��������� ��� ���������� ������:
	�������� ���������, ��� ����� �������������� ������ � ��������� �����. �� ���� ��������� �������� ��������� ����, � �� ����� ���������:

	���������� ��������
	���������� ����
	���������� ������ ����������
	���������� ����
	������� ����� �����
	����� ���������� ������
*/

#include <iostream>
#include <cstdio>
#include <stdlib.h>
#include <string>
#include <ctype.h>
#include <locale>

using namespace std;

struct FILEDATA
{
	int howm_symb;
	int famous;
	int howm_dig;
	int howm_punct;
	int howm_word;
	float avegleng;
};

void print_str(struct FILEDATA *ptr)
{
	printf("���-�� ��������: %d\n", ptr->howm_symb);
	printf("����� ���������� ������: %c\n", ptr->famous);
	printf("���-�� �����: %d\n", ptr->howm_dig);
	printf("���-�� ������ ����������: %d\n", ptr->howm_punct);
	printf("���-�� ����: %d\n", ptr->howm_word);
	printf("������� ����� �����: %.2f\n", ptr->avegleng);
}
int main()
{
	setlocale(LC_ALL, "rus");
	int arr[256] = {0};
	char name[50];
	struct FILEDATA file1;
	FILE *file;
	file1.howm_symb = 0; file1.famous = 0; file1.howm_dig = 0;
	file1.howm_punct = 0; file1.howm_word = 0; file1.avegleng = 0;
	printf("Enter name of file");
	fgets(name, 50, stdin);
	name[strlen(name) - 1] = '\0';

	file = fopen(name, "r");
	if (file == NULL)
	{
		perror("ERROR. FILE NOT FOUND!");
		exit(1);
	}

	int ch, flag, inWord = 0, wordlen = 0, temp = 0, famous = -1;
	while ((ch = fgetc(file)) != EOF)
	{
		file1.howm_symb++;
		arr[ch]++;
		if (isdigit(ch))
		{
			file1.howm_dig++;
		}
		if (ispunct(ch))
		{
			file1.howm_punct++;
		}
		if ((ch != ' ') && (ch != '\t') && (ch != '\n') && (inWord == 0))
		{
			inWord = 1;
			file1.howm_word++;
			wordlen = 1;
		}
		else if ((ch != ' ') && (ch != '\t') && (ch != '\n') && (inWord == 1))
		{
			wordlen++;
			flag = ch;
		}
		else if ((ch == ' ') | (ch == '\t') | (ch == '\n') && inWord == 1)
		{
			temp = temp + wordlen;
			inWord = 0;
			flag = 0;
		}
	}
	if (flag != 0)
		temp = temp + wordlen;
	file1.avegleng = temp / file1.howm_word;
	for (int i = 0; i<256; i++)
	{
		if (arr[i]>famous)
		{
			famous = arr[i];
			file1.famous = i;
		}
	}
	print_str(&file1);
	return 0;
}