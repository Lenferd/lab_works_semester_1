/*
	��������� ��� ���������� ������:
	�������� ���������, ��������� �� ����� ��������� ����� �� ��������� ������������� ������.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define MAXSTRING 256

int RandomNumber(int range);	//�������, �������� ��������� �����
int SizeOfString(char *string);	//�������, �������� ������ ������
void WordPrint(char *string, int wordnumber);	//�������, ��������� �������� ����� �����

int main()
{
	int range, randnum;
	char string[MAXSTRING];

	printf("Enter string for processing\n\n");
	fgets(string, MAXSTRING, stdin);
	range=SizeOfString(string);		//�������� ���-�� ���� � ������
	randnum=RandomNumber(range);				//�������� ��������� ����� (��������)
	WordPrint(&string[0], randnum);						//������� ������ ��� �����
	return 0;
}

int SizeOfString(char *string)
{
	int range;
	int WordCounter=0;
	int i, symbol, prevsymbol=0;
	for (i=0; string[i]!='\n'; i++)
	{
		if (string[i]==' ')		
		{
			symbol=0;
		}
		else					
		{
			symbol=1;
			if (prevsymbol==0)	
			{
				WordCounter++;
			}	
		}
		prevsymbol=symbol;
	}
	return (WordCounter);
}

int RandomNumber(int range)
{
	int randnum;
	srand(time(0));
	randnum=rand()%range+1;
	return(randnum);
}

void WordPrint(char *string, int wordnumber)
{
	int i, symbol, prevsymbol=0, WordCounter=0;
	printf("\nRandom word: ");
	for (i=0; string[i]!='\n'; i++)
	{
		if (string[i]==' ')		
		{
			symbol=0;
		}
		else					
		{
			symbol=1;
			if (prevsymbol==0)	
			{
				WordCounter++;
			}	
			if (WordCounter==wordnumber) 			{
				printf("%c", string[i]);
			}
		}
		prevsymbol=symbol;
	}
	printf("\n");
}
