/*
	��������� ��� ���������� ������:
	�������� ���������, �������������� ���������� ���� �� ��������� ������. 
	����� ����������� ����� ����������� ��������. 
	����������: ����������� ���������� ������.
*/
#include <stdio.h>
#include <string.h>

#define MAXSTRING 256		//���� ����� ������ 

int main()
{
	char string[MAXSTRING];
	int i, wordcounter=0;
	int symbol, prevsymbol=0;			//symbol=1 ���������� ��� ������� ������� �� ������, prevsymbol �������� �� ���������� �������
	
	printf("Enter string for processing\n");
	fgets(string,MAXSTRING,stdin);

	for (i=0; string[i]!='\n'; i++)
	{
		if (string[i]==' ')		
		{
			symbol=0;
		}
		else				
		{
			symbol=1;
			if (prevsymbol==0)	//���������, ������ �� ����� 
			{
				wordcounter++;		
			}	
		}
		prevsymbol=symbol;
	}

	printf("\nIn this string %d words\n\n", wordcounter);
	return 0;
}