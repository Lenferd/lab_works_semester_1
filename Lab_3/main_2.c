/*
	��������� ��� ���������� ������:
	�������� ���������, ������������ ����� ������� ����� �� ��������� ������. 
	������� ���������� ������ 1.
*/

#include <stdio.h>
#include <string.h>

#define MAXSTRING 256		//���� ����� ������ 

int main()
{
	char string[MAXSTRING];
	int i;
	int symbol, prevsymbol=0;			
	int max_word=0, wordlength=0;

	printf("Enter string for processing\n\n");
	fgets(string,MAXSTRING,stdin);
	
	for (i=0; string[i]!='\n'; i++)
	{
		if (string[i]==' ')		
		{
			symbol=0;
			wordlength=0;
		}
		else				
		{
			symbol=1;
			wordlength++;
		}
		prevsymbol=symbol;
		if (wordlength>max_word)
		{
			max_word=wordlength;
		}
	}
	
	printf("\nMax string have %d symbols\n\n", max_word);
	return 0;
}