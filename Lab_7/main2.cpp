/*
	������ � ������� ������ Circle ��������� ������:
	���������� ���������� ��������� ���������� ��� �������� ������� ������ �������� ��������, � ����� ��������� ���������� ������ ������ �������� (������ � ��������).
	��������� 1 ����������� ����� ��������� �������� 1000 �. 
	��������� 1 ��������� ����� ������ 2000 �. 
	������ �������� 3 �. 
	������ �������� ������� ������ �������� 1 �.
*/

#include <stdio.h>
#include "Circle.h"

const int POOLRAD=3;
const int LANERAD=1;
const int COVERPRICE=1000;
const int FENCEPRICE=2000;

int main()
{ 
	Circle PoolandLane;
	PoolandLane.setRadius(POOLRAD + LANERAD);
	PoolandLane.calcFerence_Area();
	Circle Pool;
	Pool.setRadius(POOLRAD);
	Pool.calcFerence_Area();
	printf("Track cover price: %.2f p\n", (PoolandLane.getArea() - Pool.getArea()) * COVERPRICE);
	printf("Fence price: %.2f p\n", PoolandLane.getFerence() * FENCEPRICE);
	return 0;
}