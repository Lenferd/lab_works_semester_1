class Circle
{
private:
	double radius;
	double ference;
	double area;
public:
	void setRadius(double value);
	void setFerence(double value);
	void setArea(double value);
	void calcFerence_Area();
	void calcRadius_Area();
	void calcRadius_Ference();
	double getRadius();
	double getFerence();
	double getArea();
};
