/*
	����������� ����� Circle (����), ���������� ��������� ����:

	Radius - ������
	Ference - ����� ����������
	Area - ������� �����
	������ ���� ������������� �������� ��� ���������� ���� double � � ������������� ������� private ��� ������������. ������ � ���� ����� ������� ���������� � ������� ������� ������.


	��� ��������� �������� ������� ������������� ����� ���������� � ������;
	��� ��������� ����� ���������� ������������� ������ � �������;
	��� ��������� ������� ������������� ������ � ����� ����������.
*/

#include "Circle.h"
#include <math.h>

const double PI=3.14159265359;

void Circle::setRadius(double value)
{
	radius = value;
}
void Circle::setFerence(double value)
{
	ference = value;
}
void Circle::setArea(double value)
{
	area = value;
}
void Circle::calcFerence_Area()
{
	ference = 2 * PI * radius;
	area = PI * radius * radius;
}
void Circle::calcRadius_Area()
{
	radius = ference / (2 * PI);
	area = PI * radius * radius;
}
void Circle::calcRadius_Ference()
{
	radius = sqrt(area / PI);
	ference = 2 * PI * radius;
}
double Circle::getRadius()
{
	return radius;
}
double Circle::getFerence()
{
	return ference;
}
double Circle::getArea()
{
	return area;
}
