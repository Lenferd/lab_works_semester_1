/*
	��������� ��� ���������� ������:
	������� ������ �� ����� � ��������� �������.
*/
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main()
{
	char **data;
	char *tmp;
	int N, i;
	int randnum1, randnum2;
	printf("Enter strings for processing\n\n");
	data = StringReader(&N);		//�������� ������ StringReader �������� � ����� StringReader.c
	srand(time(0));
	for (i = 0; i < N; i++)
	{
		randnum1 = rand() % N;
		randnum2 = rand() % N;
		tmp = data[randnum1];
		data[randnum1] = data[randnum2];
		data[randnum2] = tmp;
	}
	for (i = 0; i < N; i++)
		printf("%s", data[i]);
	return 0;
}