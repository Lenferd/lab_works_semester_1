/*
	��������� ��� ���������� ������
	������� ������ �� ����� � �������� �������.
*/
#include <stdio.h>

//#include "String_Reader.h"

int main()
{
	char **data;
	int N, i;
	printf("Enter strings for processing\n\n");
	data = StringReader(&N);		//�������� ������ StringReader �������� � ����� StringReader.c
	if (N>=0)
	{
		printf ("String in reverse order\n\n");
		for (i=N; i>=0; i--)
		{
			printf("%s", data[i]);
		}
		printf("\n");
	}
	else
	{
		printf("Nothing to display");
	}
	return 0;
}